using BookLib.Core.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using BookLib.Core.Models;
using BookLib.Core.Services;

namespace Tests
{
    public class Tests
    {
        [Test]
        public async Task AddBook_Success()
        {
            // Arrange
            var mockBookRepo = new Mock<IBookRepository>();
            var mockUserRepo = new Mock<IUserRepository>();
            var mockIdentity = new Mock<IIdentityService>();
            mockBookRepo.Setup(x => x.Add(It.IsAny<Book>())).ReturnsAsync("is_book_isbn");
            mockUserRepo.Setup(x => x.AddBook(It.IsAny<string>(), It.IsAny<string>()));
            mockIdentity.Setup(x => x.GetUserIdentity()).Returns("is_user_id");
            var bookService = new BookService(mockBookRepo.Object, mockUserRepo.Object, mockIdentity.Object);

            var newBook = new Book();
            var now = DateTime.Now;

            // Act
            var isbn = await bookService.Add(newBook);

            // Assert
            Assert.AreEqual("is_book_isbn", isbn);
            Assert.AreEqual("is_user_id", newBook.UserId);
            Assert.AreEqual(now.Date, newBook.Created.Date);
            Assert.AreEqual(newBook.LastUpdated, newBook.Created);
            mockBookRepo.Verify(r => r.Add(newBook), Times.Once());
            mockUserRepo.Verify(r => r.AddBook("is_user_id", "is_book_isbn"), Times.Once());
        }
    }
}