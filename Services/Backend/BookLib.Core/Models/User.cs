﻿using Microsoft.AspNetCore.Identity;

namespace BookLib.Core.Models
{
    public class User : IdentityUser
    {
        public List<string> BookISBNs { get; set; } = new();
    }
}
