﻿namespace BookLib.Core.Models
{
    public class Book
    {
        public string? Id { get; set; }
        public string ISBN { get; set; }
        public string Title { get; set; }
        public string Authors { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastUpdated { get; set; }
        public string? UserId { get; set; }
    }
}
