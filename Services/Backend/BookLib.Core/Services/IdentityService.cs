﻿using Microsoft.AspNetCore.Http;

namespace BookLib.Core.Interfaces
{
    public class IdentityService : IIdentityService
    {
        private IHttpContextAccessor _context;

        public IdentityService(IHttpContextAccessor context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public string GetUserName()
        {
            return _context.HttpContext.User.Identity?.Name;
        }

        public string GetUserIdentity()
        {
            return _context.HttpContext.User.FindFirst("sub")?.Value;
        }
    }
}
