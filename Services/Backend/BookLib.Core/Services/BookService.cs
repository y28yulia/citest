﻿using BookLib.Core.Interfaces;
using BookLib.Core.Models;
using Microsoft.AspNetCore.Identity;

namespace BookLib.Core.Services
{
    public class BookService
    {
        private readonly IBookRepository _bookRepository;
        private readonly IUserRepository _userRepository;
        private readonly IIdentityService _identityService;

        public BookService(IBookRepository bookRepository, IUserRepository userRepository, IIdentityService identityService)
        {
            _bookRepository = bookRepository;
            _userRepository = userRepository;
            _identityService = identityService;
        }

        public async Task<string> Add(Book book)
        {
            var userId = _identityService.GetUserIdentity();
            book.Created = DateTime.Now;
            book.LastUpdated = book.Created;
            book.UserId = userId;
            var addedBookIsbn = await _bookRepository.Add(book);
            await _userRepository.AddBook(userId, addedBookIsbn);
            return addedBookIsbn;
        }

        public async Task<bool> Delete(string userId, string isbn)
        {
            var isDeleted = await _bookRepository.Delete(userId, isbn);
            await _userRepository.DeleteBook(userId, isbn);
            return isDeleted;
        }

        public async Task<List<Book>> GetAll(string userId)
        {
            var books = await _bookRepository.GetAll(userId);
            return books;
        }

        public async Task<string> Set(string isbn, Book book)
        {
            var userId = _identityService.GetUserIdentity();
            var bookInDb = await _bookRepository.Get(userId, isbn);
            if (bookInDb == null) return null;
            book.Id = bookInDb.Id;
            book.Created = book.Created;
            book.LastUpdated = DateTime.Now;
            book.UserId = userId;
            var updatedBookId = await _bookRepository.Set(isbn, book);
            return updatedBookId;
        }
    }
}
