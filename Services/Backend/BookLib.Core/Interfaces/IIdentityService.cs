﻿namespace BookLib.Core.Interfaces
{
    public interface IIdentityService
    {
        string GetUserName();
        string GetUserIdentity();
    }
}
