﻿namespace BookLib.Core.Interfaces
{
    public interface IUserRepository
    {
        Task<List<string>> GetUserBookISBNs(string userId);
        Task AddBook(string userId, string isbn);
        Task DeleteBook(string userId, string isbn);
    }
}
