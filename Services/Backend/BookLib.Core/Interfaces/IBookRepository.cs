﻿using BookLib.Core.Models;

namespace BookLib.Core.Interfaces
{
    /// <summary>
    /// Интерфейс репозитория с книгами
    /// </summary>
    public interface IBookRepository
    {
        Task<Book> Get(string userId, string isbn);
        Task<List<Book>> GetAll(string userId);
        Task<bool> Delete(string userId, string isbn);
        Task<string> Set(string isbn, Book book);
        Task<string> Add(Book book);
    }
}
