﻿using BookLib.Core.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BookLib.Repositories.PostgreSqlRepository
{
    public class UserRepository : IUserRepository
    {
        private readonly AppDbContext _dbContext;

        public UserRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddBook(string userId, string isbn)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(u => u.Id == userId);
            user?.BookISBNs.Add(isbn);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteBook(string userId, string isbn)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(u => u.Id == userId);
            user?.BookISBNs.Remove(isbn);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<string>> GetUserBookISBNs(string userId)
        {
            var user = await _dbContext.Users.AsNoTracking().FirstOrDefaultAsync(u => u.Id == userId);
            var isbns = user?.BookISBNs;
            return isbns;
        }
    }
}
