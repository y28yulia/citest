﻿using AutoMapper;
using BookLib.Core.Models;
using BookLib.Repositories.MongoRepository.Model;

namespace BookLib.Repositories.MongoRepository.Mapper
{
    public class BookProfile : Profile
    {
        public BookProfile()
        {
            CreateMap<Book, DbBook>().ReverseMap();
        }
    }
}
