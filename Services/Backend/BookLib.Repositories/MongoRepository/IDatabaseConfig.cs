﻿namespace BookLib.Repositories.MongoRepository
{
    public interface IDatabaseConfig
    {
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
