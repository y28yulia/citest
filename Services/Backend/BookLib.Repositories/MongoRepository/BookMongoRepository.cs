﻿using AutoMapper;
using BookLib.Core.Interfaces;
using BookLib.Core.Models;
using BookLib.Repositories.MongoRepository.Model;
using MongoDB.Bson;
using MongoDB.Driver;

namespace BookLib.Repositories.MongoRepository
{
    public class BookMongoRepository : IBookRepository
    {
        private readonly IMongoCollection<DbBook> _books;
        private readonly IMapper _mapper;

        public BookMongoRepository(IDatabaseConfig config, IMapper mapper)
        {
            _mapper = mapper;

            var client = new MongoClient(config.ConnectionString);
            var database = client.GetDatabase(config.DatabaseName);
            _books = database.GetCollection<DbBook>("books");
        }

        public async Task<string> Add(Book book)
        {
            var dbBook = _mapper.Map<DbBook>(book);
            var bookInDb = await GetBookByUserAndIsbn(book.UserId, book.ISBN);
            if (bookInDb != null)
            {
                throw new ArgumentException($"Книга с ISBN {book.ISBN} уже в базе данных");
            }
            await _books.InsertOneAsync(dbBook);

            return dbBook.ISBN;
        }

        public async Task<bool> Delete(string userId, string isbn)
        {
            var result = await _books.DeleteOneAsync(x => x.ISBN == isbn && x.UserId == userId);
            return result.DeletedCount > 0;
        }

        public async Task<Book> Get(string userId, string isbn)
        {
            var dbBook = await GetBookByUserAndIsbn(userId, isbn);
            var book = _mapper.Map<Book>(dbBook);

            return book;
        }

        public async Task<List<Book>> GetAll(string userId)
        {
            var dbBooks = await _books.Find(x => x.UserId == userId).ToListAsync();
            var books = dbBooks.Select(book => _mapper.Map<Book>(book)).ToList();

            return books;
        }

        public async Task<string> Set(string isbn, Book book)
        {
            var dbNewBook = _mapper.Map<DbBook>(book);
            await _books.ReplaceOneAsync(x => x.ISBN == isbn && x.UserId == book.UserId, dbNewBook);

            return dbNewBook.ISBN;
        }

        private async Task<DbBook> GetBookByUserAndIsbn(string userId, string isbn)
        {
            var cursor = await _books.FindAsync(x => x.ISBN == isbn && x.UserId == userId);
            var dbBook = await cursor.SingleOrDefaultAsync();
            return dbBook;
        }
    }
}
