﻿using System.Reflection;
using Microsoft.AspNetCore.Mvc;

namespace BookLib.WebUi.Controllers;

[ApiController]
[Route("api/[controller]")]
public class VersionController : ControllerBase
{
    [HttpGet]
    public IActionResult Get()
    {
        return new JsonResult(Assembly.GetEntryAssembly()?.GetCustomAttribute<AssemblyInformationalVersionAttribute>()?.InformationalVersion);
    }
}