﻿using BookLib.Core.Models;
using BookLib.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BookLib.WebUi.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class BookController : ControllerBase
    {
        private readonly BookService _bookService;
        private readonly UserManager<User> _userManager;

        public BookController(BookService bookService, UserManager<User> userManager)
        {
            _bookService = bookService;
            _userManager = userManager;
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            if (User.Identity == null)
            {
                return BadRequest();
            }
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var books =  await _bookService.GetAll(user.Id);
            return new JsonResult(books);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] Book book)
        {
            if (User.Identity == null)
            {
                return BadRequest();
            }
            try
            {
                var isbn = await _bookService.Add(book);
                return Ok(isbn);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Edit(string isbn, [FromBody] Book book)
        {
            if (User.Identity == null)
            {
                return BadRequest();
            }
            var result = await _bookService.Set(isbn, book);
            return Ok(result);
        }

        [Authorize]
        [HttpDelete]
        public async Task<IActionResult> Delete(string isbn)
        {
            if (User.Identity == null)
            {
                return BadRequest();
            }
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var deleted = await _bookService.Delete(user.Id, isbn);
            return Ok(deleted);
        }
    }
}
