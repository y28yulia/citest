﻿using BookLib.Core.Interfaces;
using BookLib.Core.Models;
using BookLib.Core.Services;
using BookLib.Repositories.MongoRepository;
using BookLib.Repositories.MongoRepository.Mapper;
using BookLib.Repositories.PostgreSqlRepository;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using OpenIddict.Validation.AspNetCore;
using static OpenIddict.Abstractions.OpenIddictConstants;

namespace BookLib.WebUi
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();

            services.AddControllers();
            services.AddMvc(options => options.SuppressAsyncSuffixInActionNames = false);

            services.AddAutoMapper(typeof(BookProfile));

            services.AddTransient<IBookRepository, BookMongoRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<BookService, BookService>();
            services.AddTransient<IIdentityService, IdentityService>();

            services.AddHttpContextAccessor();

            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<DatabaseConfig>(config =>
            {
                config.ConnectionString = Configuration["Mongo:ConnectionString"];
                config.DatabaseName = Configuration["Mongo:DatabaseName"];
            });
            services.AddSingleton<IDatabaseConfig>(x => x.GetRequiredService<IOptions<DatabaseConfig>>().Value);

            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseNpgsql(Configuration["PostgreSql:ConnectionString"]);
                options.UseOpenIddict();
            });

            services.Configure<IdentityOptions>(options =>
            {
                options.ClaimsIdentity.UserNameClaimType = Claims.Name;
                options.ClaimsIdentity.UserIdClaimType = Claims.Subject;
                options.ClaimsIdentity.RoleClaimType = Claims.Role;
                options.ClaimsIdentity.EmailClaimType = Claims.Email;
            });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = OpenIddictValidationAspNetCoreDefaults.AuthenticationScheme;
            });

            services
                .AddOpenIddict()
                .AddCore(options =>
                {
                    options.UseEntityFrameworkCore()
                           .UseDbContext<AppDbContext>();
                })
                .AddServer(options =>
                {
                    options.SetAuthorizationEndpointUris("/api/connect/authorize");
                    options.SetTokenEndpointUris("/api/connect/token");
                    options.AllowAuthorizationCodeFlow();
                    options.AllowPasswordFlow();
                    options.AllowRefreshTokenFlow();
                    options.AddDevelopmentEncryptionCertificate()
                           .AddDevelopmentSigningCertificate();
                    options.RegisterScopes(Scopes.Profile, Scopes.Roles, Scopes.OfflineAccess);
                    options.UseAspNetCore()
                            .EnableStatusCodePagesIntegration()
                            .EnableAuthorizationEndpointPassthrough()
                            .EnableLogoutEndpointPassthrough()
                            .EnableTokenEndpointPassthrough()
                            .EnableUserinfoEndpointPassthrough()
                           .DisableTransportSecurityRequirement();
                })
                .AddValidation(options =>
                {
                    options.UseLocalServer();
                    options.UseAspNetCore();
                });

            services.AddCors();
            services.AddHostedService<Worker>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });
        }
    }
}
