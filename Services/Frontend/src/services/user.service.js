import axios from 'axios'

const config = {
  grant_type: 'password',
  client_id: 'booklib_app',
  client_secret: 'qweqdasd@Dq2d2f23g23g',
  scope: 'openid profile roles offline_access'
};

function login(user) {
  const params = new URLSearchParams()
  params.append('client_id', config.client_id)
  params.append('client_secret', config.client_secret)
  params.append('grant_type',  config.grant_type)
  params.append('scope',  config.scope)
  params.append('username', user.username)
  params.append('password', user.password)
  return axios
    .post(process.env.VUE_APP_BOOK_URL + '/api/connect/token', params, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Allows': 'application/x-www-form-urlencoded'
      }
    })
    .then(response => {
      const user = response.data;
      if (user.access_token) {
        localStorage.setItem('user', JSON.stringify(user));
        return user;
      } else {
        localStorage.removeItem('user');
        return null;
      }
    })
    .catch(error => {
      this.result = error.response.status;
    });
}

function logout() {
    localStorage.removeItem('user');
}

export const userService = {
  login,
  logout
};
